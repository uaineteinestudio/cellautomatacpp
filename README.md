# CellAutomataC++
A c++ cell automata algorithm complete with floodfiller for chest placement. Works using the 4 connection method.

## Getting Started

* Note this is handled with static memory for performance where the maximum length is given by the maplen.h file.

See the CellularAutomatacpp.cpp file for execution examples.

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Built With

* [Floodfiller](https://bitbucket.org/uaineteinestudio/floodfiller) by [UaineTeine](https://bitbucket.org/uaineteinestudio) - Used to find the size of the current chamber to ensure it is appropriate for chest placement.