#include "cellautomawater.h"

CAWater::CAWater()
{
	setNormal();
}

CAWater::~CAWater()
{
}

void CAWater::setIslands()
{
	chanceToStartAlive = 0.4;
	deathlimit = 3;
	birthlimit = 4;
	noSteps = 3;
}

void CAWater::setNormal()
{
	chanceToStartAlive = 0.36f;
	deathlimit = 2;
	birthlimit = 3;
	noSteps = 6;
}
