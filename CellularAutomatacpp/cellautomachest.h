#pragma once

#include "cellautoma.h"
#include <vector>

using namespace std;

class CAChest : public CA//cell autonma chest class
{
public:
	CAChest();//default constructor
	// chance to start alive, death lim, birth lim, simsteps, 
	// treasure limit, num chests
	CAChest(float ch, int dl, int bl, int smoofac, int tL, int nC) : CA(ch, dl, bl, smoofac) 
	{ 
		treasureLim = tL;
		numChests = nC;
	}
	~CAChest();
	static const int maxChests = 20;
	int getNumChests();
	//1 and 0 here represent y and x
	int** GetTreasureSpots(bool** map, int width, int height);				//returns 2d array of the [n][0] and [n][1] spot for placement
	int getChamberSize(bool** map, int xi, int yi, int width, int height);	//get chamber size from floodfiller
protected:
	int treasureLim;//alive count to be a chest
	int numChests;
private:
	bool validTreasurePlacement(bool** map, int xi, int yi, int width, int height, int &numrefs); //map by ref and return num of refs at point please
	int minChamberSize = 2;
};
