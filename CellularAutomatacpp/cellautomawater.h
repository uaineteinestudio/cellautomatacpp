#pragma once

#include "cellautoma.h"
#include <vector>

using namespace std;

class CAWater : public CA//cell autonma chest class
{
public:
	CAWater();//default constructor
	// chance to start alive, death lim, birth lim, simsteps
	CAWater(float ch, int dl, int bl, int smoofac) : CA(ch, dl, bl, smoofac) { }
	~CAWater();
	void setIslands();
	void setNormal();
};
