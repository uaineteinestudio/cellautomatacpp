// CellularAutomatacpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include "cellautoma.h"
#include "cellautomachest.h"

using namespace std;

void print_matrix(bool** matrix, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
}

void writeToFile(bool** map, int width, int height, string fn)
{
	ofstream myfile;
	myfile.open(fn);
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			if (map[x][y])
				myfile << "1";
			else
				myfile << "0";
			if (x == width - 1)
				myfile << "\n";
			else
				myfile << ",";
		}
	}
	myfile.close();
	cout << "written to file" << endl;
}

void caTest()
{
	int width = 20;
	int height = 20;
	CA cella = CA();
	while (true)
	{
		bool** map = cella.cellautomata(width, height);

		print_matrix(map, width, height);
		//writeToFile(map, width, height, "test.txt");
		system("PAUSE");

		//delete array

		for (int x = 0; x < width; x++)
		{
			delete[] map[x];
		}
		delete[] map;
	}
}

void caChestTest()
{
	int width = 20;
	int height = 20;
	CAChest cella = CAChest();
	while (true)
	{
		bool** map = cella.cellautomata(width, height);

		print_matrix(map, width, height);
		//writeToFile(map, width, height, "test.txt");

		//now get the locations
		int** treasureSpots = cella.GetTreasureSpots(map, width, height);
		cout << "Chest loations are:" << endl;
		for (int i = 0; i < cella.getNumChests(); i++)
		{
			cout << treasureSpots[i][1] << " , " << treasureSpots[i][0] << endl;
		}
		system("PAUSE");


		//delete arrays
		for (int x = 0; x < width; x++)
		{
			delete[] map[x];
		}
		delete[] map;
	}
}

int main()
{
    std::cout << "Hello World!\n";
	//caTest();
	caChestTest();
	system("PAUSE");
	return 0;
}