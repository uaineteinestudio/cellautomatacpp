#include "cellautomachest.h"
#include "floodfillersize.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

CAChest::CAChest()
{
	//keeping as is for the other deets
	//adding this below
	treasureLim = 5;
	numChests = 3;
}

CAChest::~CAChest()
{
}

int CAChest::getNumChests()
{
	return numChests;
}

int ** CAChest::GetTreasureSpots(bool ** map, int width, int height)
{
	//initalise
	vector<vector<int>> treasureSpots;
	int n = 0;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int numrefs = 0;//number of neighbours
			if (validTreasurePlacement(map, x, y, width, height, numrefs))//if valid
			{
				vector<int> spot;
				spot.push_back(x);
				spot.push_back(y);
				treasureSpots.push_back(spot);
				n += 1;
			}
		}
	}
	//now filter by random choice
	int finalsize = numChests;
	if (n <= numChests)
		finalsize = n;
	//intialise final list
	int** finalList = 0;
	finalList = new int*[finalsize];
	for (int x = 0; x < finalsize; x++)
	{
		finalList[x] = new int[2];		//(0 and 1)
		finalList[x][0] = -1;
		finalList[x][1] = -1;
	}
	//now produce
	if (n <= numChests)
	{
		for (int i = 0; i < finalsize; i++)
		{
			finalList[i][0] = treasureSpots[i][0];
			finalList[i][1] = treasureSpots[i][1];
		}
	}
	else
	{
		/* initialize random seed: */
		srand(time(NULL));
		//need random ones
		for (int i = 0; i < finalsize; i++)
		{
			int k = rand() % (treasureSpots.size() - 1);//get random index
			finalList[i][0] = treasureSpots[k][0];
			finalList[i][1] = treasureSpots[k][1];
			//now delete from vector thanks
			treasureSpots.erase(treasureSpots.begin() + k, treasureSpots.begin() + k + 1);
		}
	}
	//now return
	return finalList;
}

int CAChest::getChamberSize(bool ** map, int xi, int yi, int width, int height)
{
	floodfillersize fs = floodfillersize(width, height);
	bool dummyVal = !map[xi][yi];
	return fs.getSize(map, xi, yi, dummyVal);
}

bool CAChest::validTreasurePlacement(bool ** map, int xi, int yi, int width, int height, int & numrefs)
{
	if (map[xi][yi]) //is a blocked out cell
	{
		int chSize = getChamberSize(map, xi, yi, width, height);
		if (chSize >= minChamberSize)
		{
			numrefs = countAliveNeighbours(map, xi, yi, width, height);
			if (numrefs >= treasureLim)
				return true;
		}
	}
	return false;
}
