#pragma once

#include "floodfillersize.h"
#include "maplen.h"

using namespace std;

class CA//cell autonma class
{
public:
	CA();//default constructor
	//chance to start alive, death lim, birth lim, sim steps
	CA(float ch, int dl, int bl, int smoofac);
	~CA();
	//-cell automata	
	bool** cellautomata(int width, int height);					//2D array of pointers
protected:
	float chanceToStartAlive = 0.32f;							//float to start alive
	int deathlimit = 5;
	int birthlimit = 4;
	int noSteps = 2;
	int countAliveNeighbours(bool** map, int x, int y, int width, int height);
	bool** simulationStep(bool** oldmap, int width, int height);	//pass by ref
};
